try:
    import unzip_requirements
except ImportError:
    pass

import base64
import json
import logging

from message_processor import MessageProcessor

FORMAT = "[%(asctime)s] %(levelname)s %(filename)s %(funcName)s:%(lineno)d %(message)s"
logger = logging.getLogger()
logger.setLevel(logging.INFO)
fmt = logging.Formatter(FORMAT)
try:
    logger.handlers[0].setFormatter(fmt)
except IndexError:
    print("\nLogging handlers empty")

message_processor = MessageProcessor()


# Method for handling jira changes
def handle(event):
    decoded_record_data = [base64.b64decode(record['kinesis']['data']) for record in event['Records']]
    deserialized_data = [json.loads(decoded_record) for decoded_record in decoded_record_data]

    for item in deserialized_data:
        logger.info(item)
        try:
            if message_processor.is_handled():
                message_processor.process_message("", "", "", "", 5, item)
            else:
                logger.info("Skipping message.")
        except Exception as e:
            logging.exception("failed: {}".format(e))
