# Name: message_processor.py
# Created: 15/08/2018
# Author: Connor Hill
# Description - python file to handle messages from webhook events

try:
    import unzip_requirements
except ImportError:
    pass

import json
import logging
import os
import re

from SPARQLWrapper import SPARQLWrapper, BASIC, JSON
from rdflib import Literal, XSD

# Database information
url = "http://knowledgebase.sword-apak.com:7200/repositories/apak/statements"
username = ''
password = ''

# Logging set-up
FORMAT = "[%(asctime)s] %(levelname)s %(filename)s %(funcName)s:%(lineno)d %(message)s"
logger = logging.getLogger()
logger.setLevel(logging.INFO)
fmt = logging.Formatter(FORMAT)
try:
    logger.handlers[0].setFormatter(fmt)
except IndexError:
    print("\nLogger has no handler")

custom_mappings = {
    'customfield_11184': 'Testing Guidelines',
    'customfield_11176': 'Impact on Product',
    'customfield_11183': 'Acceptance Criteria',
    'customfield_11172': 'Benefits of Change',
    'customfield_11170': 'Reason for Change',
    'customfield_11175': 'Risk to Customer',
    'customfield_10230': 'Client Code',
    'customfield_11161': 'Product',
    'customfield_11166': 'Commit Versions',
    'customfield_11174': 'Implication of No Change',
    'customfield_11173': 'Value to Customer',
    'customfield_12262': 'Release Note Type',
    'customfield_12260': 'Commit Span Start',
    'customfield_12261': 'Commit Span End',
    'customfield_12060': 'User Story Link',
    'customfield_10055': 'Quote',
    'customfield_11171': 'Describe how content will satisfy Objectives',
    'customfield_11178': 'Effective Quality',
    'customfield_10050': 'Instructions',
    'customfield_10093': 'Post Release Checks',
    'customfield_11179': 'Post Release Client Feedback',
    'customfield_11180': 'Post Release Client Manager Feedback',
    'customfield_11182': 'Billed Value',
    'customfield_10059': 'Impact',
    'customfield_12367': 'Risk Rating',
    'customfield_10031': 'Client Reference'
}


class MessageProcessor:

    # Name: is_handled
    # Parameters: self
    # Returns: True
    # Description: Method that checks whether a message is handled
    def is_handled(self):
        return True

    # Name: process_message
    # Parameters: self, host (String), repo (String), username (String), password (String), timeout (int), message (json)
    # Returns: None
    # Description: Method that takes a message and relevant parameters and then processes it accordingly
    def process_message(self, host, repo, username, password, timeout, message):
        output = self.produce_ouput(message)

        # Determine which JIRA data is being received
        # Service Desk Release JIRA Data
        if output["projectCategory"]["name"] in ("WFSv6 Clients", "Aurius Clients", "WFSv5") and output["issueType"]["name"] == "Release":
            config_path = "../rdfConfig/release.json"

        # Service Desk Requirement JIRA Data
        elif output["projectCategory"]["name"] in ("WFSv6 Clients", "Aurius Clients", "WFSv5") and output["issueType"]["name"] != "Release":
            config_path = "../rdfConfig/requirement.json"

        # Design JIRA Data
        elif output["project"]["key"] == "PROD":
            config_path = "../rdfConfig/design.json"

        # V6 / Aurius / V5 Development JIRA Data
        elif output["projectCategory"]["name"] in ("WFSv6 System", "Aurius", "WFSv5 System"):
            config_path = "../rdfConfig/change.json"
        else:
            logger.info("Not interested in this change.")

        # Determine which webhook event is being received
        if output["webhookEvent"] == "jira:issue_updated":
            output["changelog"] = message["changelog"]
            self.handle_updates(config_path, output, message)
        elif output["webhookEvent"] == "jira:issue_created":
            self.handle_creation(config_path, output, message)
        elif output["webhookEvent"] == "jira:issue_deleted":
            self.handle_deletion(config_path, output, message)
        else:
            logger.info("Unrecognised webhook event: ", output["webhookEvent"])

    # Name: produce_output
    # Parameters: self, message (json)
    # Returns: output (json)
    # Description: Method that takes a payload then extracts and returns the required information
    def produce_ouput(self, message):
        return {
            "webhookEvent": message["webhookEvent"],
            "issueType": message["issue"]["fields"]["issuetype"],
            "projectCategory": message["issue"]["fields"]["project"]["projectCategory"],
            "project": message["issue"]["fields"]["project"],
            "key": message["issue"]["key"],
        }

    # Name: handle_creation
    # Parameters: self, config_path (String), output (json), message (json)
    # Returns: None
    # Description: Method that uses the relevant config file and field mappings to process and handle jira creations
    def handle_creation(self, config_path, output, message):
        if os.path.exists(config_path):
            config = json.load(open(config_path))
        else:
            logger.info("{} is not a valid configuration path.".format(config_path))

    # Name: handle_deletion
    # Parameters: self, config_path (String), output (json), message (json)
    # Returns: None
    # Description: Method that uses the relevant config file and field mappings to process and handle jira deletions
    def handle_deletion(self, config_path, output, message):
        if os.path.exists(config_path):
            config = json.load(open(config_path))
        else:
            logger.info("{} is not a valid configuration path.".format(config_path))

    # Name: handle_updates
    # Parameters: self, config_path (String), output (json), message (json)
    # Returns: None
    # Description: Method that uses the relevant config file and field mappings to process and handle jira updates
    def handle_updates(self, config_path, output, message):
        changes = []

        for current_change in output["changelog"]["items"]:
            new_change = []

            # Check if changed field is a custom field and map accordingly
            if current_change["field"] in custom_mappings:
                change_field = custom_mappings[current_change["field"]]
            else:
                change_field = current_change["field"]

            new_change.append(change_field)
            new_change.append(current_change["toString"])
            new_change.append(current_change["fromString"])
            changes.append(new_change)

        # Load config information
        if os.path.exists(config_path):
            config = json.load(open(config_path))
            subject_field = config["subject"]["subjectField"]

            # Check if subject field has multiple values and handle accordingly
            if config["subject"]["multi"]:
                subject_list = self.handle_multi(subject_field, message)

                # Remove white space from subject field
                for i in range(len(subject_list)):
                    subject_list[i] = re.sub(r"\s+", "", subject_list[i])

                for subject_field in subject_list:
                    for change in changes:
                        subject, predicate, obj = self.build_triple(config, change, subject_field)
                        sparql_query = self.create_sparql_query(subject, predicate, obj)
                        sparql_query.queryAndConvert()

                        # Handle labels
                        if "label" in config["subjectFields"][change[0]]:
                            predicate = "http://www.w3.org/2000/01/rdf-schema#label"
                            obj = Literal(change, datatype=XSD.string)
                            sparql_query = self.create_sparql_query(subject, predicate, obj)
                            sparql_query.queryAndConvert()
            else:
                for change in changes:
                    subject_field = re.sub(r"\s+", "", subject_field)
                    subject, predicate, obj = self.build_triple(config, change, subject_field)
                    sparql_query = self.create_sparql_query(subject, predicate, obj)
                    sparql_query.queryAndConvert()
        else:
            logger.info("{} is not a valid configuration path.".format(config_path))

    # Name: build_triple
    # Parameters: self, config (json), change (String), subject_field (String)
    # Returns: subject (URI), predicate (URI), object (URI/Literal)
    # Description: Method that builds the corresponding triple using the config file.
    def build_triple(self, config, change, subject_field):
        # Set subject
        prefix = config["subject"]["prefix"]
        subject = "<" + prefix + subject_field + ">"
        print("\nSubject: " + str(subject))

        # Set predicate
        predicate = "<" + config["subjectFields"][change[0]]["predicate"] + ">"
        print("Predicate: " + str(predicate))

        # Set object
        object_type = config["subjectFields"][change[0]]["type"]
        if object_type == "uri":
            obj = "<" + change[1] + ">"
        else:
            obj = "'" + change[1] + "'"

        print("Object: " + str(obj))
        return subject, predicate, obj

    # Name: handle_multi
    # Parameters: self, subject_field (String), message (json)
    # Returns: subject_list (list)
    # Description: Method that handles subjects with multiple values. Returns a list of all subjects
    def handle_multi(self, subject_field, message):
        subject_path = subject_field.split(".")
        subject_list = []

        for field_name in subject_path[:-1]:
            if field_name == "*":
                for element in message:
                    subject_list.append(element[subject_path[len(subject_path) - 1]])
            else:
                message = message[field_name]
        return subject_list

    # Name: create_sparql_query
    # Parameters: self, message (json)
    # Returns: spaql (object)
    # Description: Method that creates a sparql query from passed triple
    def create_sparql_query(self, subject, predicate, obj):
        # TODO - replace with config['graph'] when no longer testing
        graph_url = "<http://www.sword-apak.com/graph#jiratest>"
        sparql = self.create_sparql_wrapper()
        sparql.method = 'POST'
        sparql.setHTTPAuth(BASIC)
        sparql.setCredentials(username, password)
        query = "with " + graph_url + " delete { " + subject + " " + predicate + " ?o } insert { " + subject + " " + predicate + " " + obj + " } where" + " { " + subject + " " + predicate + " ?o } "
        sparql.setQuery(query)
        return sparql

    # Name: create_sparql_wrapper
    # Parameters: self
    # Returns: sparql wrapper
    # Description: Method that creates and returns a sparql wrapper in json format
    def create_sparql_wrapper(self):
        sparql = SPARQLWrapper(url)
        sparql.setReturnFormat(JSON)
        return sparql
