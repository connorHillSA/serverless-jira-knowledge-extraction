import json

from message_processor import MessageProcessor

messageProcessor = MessageProcessor()


def test_message_processor_created():
    with open('resources/jira:issue_created.json') as json_data:
        message = json.load(json_data)

    output = messageProcessor.process_message('', '', '', '', 5, message)

    print("\nAssertion #1 - output matches message")
    assert output["webhookEvent"] == message["webhookEvent"]
