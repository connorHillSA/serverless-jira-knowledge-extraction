import json

from message_processor import MessageProcessor

messageProcessor = MessageProcessor()


def test_message_processor_updated_get_values():
    with open('resources/jira:issue_updated.json') as json_data:
        message = json.load(json_data)

    output = messageProcessor.process_message('', '', '', '', 5, message)

    print("\nAssertion #1 - output category matches message")
    assert output["projectCategory"] == message["issue"]["fields"]["project"]["projectCategory"]
