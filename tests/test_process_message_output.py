import json

from message_processor import MessageProcessor

messageProcessor = MessageProcessor()


def test_message_processor_output():
    with open('resources/jira:issue_updated.json') as json_data:
        message = json.load(json_data)

    output = messageProcessor.process_message('', '', '', '', 5, message)

    print("\nAssertion #1 - jira:issue_updated output matches message")
    assert output["webhookEvent"] == message["webhookEvent"]

    with open('resources/jira:issue_created.json') as json_data:
        message = json.load(json_data)

    output = messageProcessor.process_message('', '', '', '', 5, message)

    print("\nAssertion #2 - jira:issue_created output matches message")
    assert output["webhookEvent"] == message["webhookEvent"]
